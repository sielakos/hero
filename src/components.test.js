jest.mock("./issues");

const { test, expect, describe } = require("@jest/globals");
const fetch = require("node-fetch");
const { getComponents } = require("./components");
const { mockedResponse, expectRejectedPromise } = require("./test-utils");

describe("getComponents", () => {
  test("should fetch components for given project", async () => {
    fetch.mockReturnValue(
      mockedResponse([
        {
          id: "1",
          name: "test",
          project: "test-p",
        },
        {
          id: "2",
          name: "test 2",
          project: "test-p",
        },
      ])
    );

    const response = await getComponents("test-p");

    expect(response).toEqual([
      {
        id: "1",
        name: "test",
        project: "test-p",
        issuesCount: 20,
      },
      {
        id: "2",
        name: "test 2",
        project: "test-p",
        issuesCount: 20,
      },
    ]);
  });

  test("should fail if project is not a string", async () => {
    await expectRejectedPromise(
      getComponents(1),
      "Incorrect type of value string required"
    );
  });

  test("should fail if response has incorrect format", async () => {
    fetch.mockReturnValueOnce(
      mockedResponse({
        id: "1",
        name: "test",
        project: "test-p",
      })
    );

    await expectRejectedPromise(
      getComponents("test-p"),
      "Response should be an array"
    );

    fetch.mockReturnValue(
      mockedResponse([
        {
          id: "1",
          name: "test",
          project: "test-p2",
        },
        {
          id: "2",
          name: "test 2",
          project: "test-p",
        },
      ])
    );

    await expectRejectedPromise(
      getComponents("test-p"),
      "Response should have minimum required properties and project name should be correct"
    );

    fetch.mockReturnValue(
      mockedResponse([
        {
          id: "1",
          name: "test",
          project: "test-p",
        },
        {
          id: "2",
          project: "test-p",
        },
      ])
    );

    await expectRejectedPromise(
      getComponents("test-p"),
      "Response should have minimum required properties and project name should be correct"
    );

    fetch.mockReturnValue(
      mockedResponse([
        {
          id: "1",
          name: "test",
          project: "test-p",
        },
        {
          id: "2",
          name: 1,
          project: "test-p",
        },
      ])
    );

    await expectRejectedPromise(
      getComponents("test-p"),
      "Incorrect type of value string required"
    );

    fetch.mockReturnValue(
      mockedResponse([
        {
          id: "1",
          name: "test",
          project: "test-p",
        },
        {
          id: "2",
          name: "1",
          project: "test-p",
          lead: "string",
        },
      ])
    );

    await expectRejectedPromise(
      getComponents("test-p"),
      "Value needs to be an object"
    );
  });
});
