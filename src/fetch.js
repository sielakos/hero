const { assertType, assertObject } = require("./asserts");
const fetch = require("node-fetch");

const defaultFetchOptions = { method: "GET", params: {}, mode: "cors" };

/**
 * Fetches given url using Fetch API and parses response as json
 *
 * @param {string} url
 * @param {Object} options Fetch Api options with addtional params object to be formatted into url search string
 */
async function fetchJson(url, options = defaultFetchOptions) {
  assertType(url, "string");
  assertObject(options);

  let opts = Object.assign({}, defaultFetchOptions, options);

  const formattedParams = formatParams(opts.params);
  let response = await fetch(
    formattedParams.length > 0 ? `${url}?${formattedParams}` : url,
    opts
  );

  response = await response.json();

  return response;
}
exports.fetchJson = fetchJson;

/**
 * Format object into proper url search string.
 *
 * @param {Object} params
 */
function formatParams(params) {
  assertObject(params);

  const urlParams = new URLSearchParams();

  for (let [key, value] of Object.entries(params)) {
    urlParams.set(key, value);
  }

  return urlParams.toString();
}
exports.formatParams = formatParams;
