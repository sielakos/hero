const { fetchJson } = require("./fetch");
const { assert, assertType, assertObject } = require("./asserts");

function checkResponse(response) {
  assertObject(response);
  assertType(response.total, "number");
  assert(Array.isArray(response.issues), "Issues should be array");
}

function transformIssues(issues) {
  return issues
    .map((issue) => {
      assertObject(issue.fields);

      return issue.fields;
    })
    .filter(
      (issue) => Array.isArray(issue.components) && issue.components.length > 0
    );
}

async function fetchPart(project, startAt, payload) {
  const response = await fetchJson(
    "https://herocoders.atlassian.net/rest/api/3/search",
    {
      params: {
        jql: `project=${project}`,
        maxResults: payload.toString(),
        startAt,
        fields: "components",
      },
    }
  );

  checkResponse(response);

  return response;
}

async function fetchAll(project) {
  const payload = 100;
  let total = 100; // some initial value
  let issues = [];

  while (issues.length < total) {
    const response = await fetchPart(project, issues.length, payload);

    total = response.total;
    issues = issues.concat(response.issues);
  }

  return issues;
}

/**
 * Fetches all the issues from jira for given project
 *
 * @param {string} project key
 * @returns {Promise<Array<{components: Array}>>}
 */
async function getIssues(project = "IC") {
  assertType(project, "string");

  const issues = await fetchAll(project);

  return transformIssues(issues);
}
exports.getIssues = getIssues;

/**
 * Fetch count of issues for given project and component
 *
 * @param {string} project
 * @param {string} componentId
 * @returns {Promise<number>}
 */
async function getIssuesCount(project, componentId) {
  assertType(project, "string");
  assertType(componentId, "string");

  const response = await fetchJson(
    "https://herocoders.atlassian.net/rest/api/3/search",
    {
      params: {
        jql: `project = ${project} and component = ${componentId}`,
        maxResults: 0,
      },
    }
  );

  assertObject(response);
  assertType(response.total, "number");

  return response.total;
}
exports.getIssuesCount = getIssuesCount;
