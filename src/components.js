const { fetchJson } = require("./fetch");
const { assert, assertType, assertObject } = require("./asserts");
const { getIssuesCount } = require("./issues");

function checkResponse(response, project) {
  assert(Array.isArray(response), "Response should be an array");
  assert(
    response.every((row) => row.id && row.name && row.project === project),
    "Response should have minimum required properties and project name should be correct"
  );

  response.forEach(({ id, name, project, lead }) => {
    assertType(id, "string");
    assertType(name, "string");
    assertType(project, "string");

    if (lead) {
      assertObject(lead);
    }
  });
}

function getIssuesCountsForComponents(project, components) {
  return Promise.all(
    components.map(async (component) => {
      const count = await getIssuesCount(project, component.id);

      return {
        ...component,
        issuesCount: count,
      };
    })
  );
}

/**
 * Fetches components without lead for given project
 *
 * @param {string} project name of the project to fetch
 * @returns {Array<{id: string, name: string, project: string, issuesCount: number>} list of components
 */
async function getComponents(project = "IC") {
  assertType(project, "string");

  const response = await fetchJson(
    `https://herocoders.atlassian.net/rest/api/3/project/${project}/components`
  );

  checkResponse(response, project);

  const withoutLead = response.filter(({ lead }) => !lead);

  return getIssuesCountsForComponents(project, withoutLead);
}
exports.getComponents = getComponents;
