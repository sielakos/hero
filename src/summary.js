const { getComponents } = require("./components");
const { assertType } = require("./asserts");

/**
 * Prints id, name and count of issues for each component without lead assigned
 *
 * @param {string} project key of project
 */
async function printLeadlessSummary(project = "IC") {
  assertType(project, "string");

  const summary = await getComponents(project);

  for (let row of summary) {
    console.log(
      `id: ${row.id}, name: ${row.name}, issues count: ${row.issuesCount}`
    );
  }
}
exports.printLeadlessSummary = printLeadlessSummary;
