/**
 * Class representing assertion error.
 */
class AssertionError extends Error {
  /**
   * @param {string} msg error message
   */
  constructor(msg) {
    super(msg);
  }
}
exports.AssertionError = AssertionError;

/**
 * Throws error if value doesn't have required type. Otherwise returns undefined.
 *
 * @param {*} value
 * @param {string} type
 */
function assertType(value, type) {
  if (typeof type !== "string") {
    throw new AssertionError("Type needs to be string");
  }

  if (typeof value !== type) {
    throw new AssertionError(`Incorrect type of value ${type} required`);
  }
}
exports.assertType = assertType;

/**
 * Throws error if value is not instance of given class
 *
 * @param {*} value
 * @param {*} BaseClass class to test against
 * @param {string} msg optional error message
 */
function assertInstance(
  value,
  BaseClass,
  msg = "Value needs to be instance of BaseClass"
) {
  assertType(msg, "string");
  assertType(BaseClass, "function");

  if (value instanceof BaseClass) {
    return;
  }

  throw new AssertionError(msg);
}
exports.assertInstance = assertInstance;

function assertFunction(value) {
  assertInstance(value, Function, "Value needs to be a function");
}
exports.assertFunction = assertFunction;

/**
 * Throws error if value is not js object
 *
 * @param {*} value
 */
function assertObject(value) {
  const msg = "Value needs to be an object";

  assertInstance(value, Object, msg);

  if (Array.isArray(value) || value instanceof Function) {
    throw new AssertionError(msg);
  }
}
exports.assertObject = assertObject;

/**
 * Asserts that given condition is true
 *
 * @param {boolean} cond
 * @param {string} msg
 */
function assert(cond, msg = "Assertion failed") {
  assertType(msg, "string");

  if (!cond) {
    throw new AssertionError(msg);
  }
}
exports.assert = assert;
