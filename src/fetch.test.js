const { test, expect, describe } = require("@jest/globals");
const fetch = require("node-fetch");
const { fetchJson, formatParams } = require("./fetch");
const { mockedResponse } = require("./test-utils");

describe("formatParams", () => {
  test("should format params object into string", () => {
    expect(formatParams({ a: 1, b: "ala" })).toBe("a=1&b=ala");
    expect(formatParams({})).toBe("");
    expect(() => formatParams("ala")).toThrow();
    expect(() => formatParams(undefined)).toThrow();
    expect(() => formatParams(null)).toThrow();
    expect(() => formatParams([])).toThrow();
    expect(() => formatParams(() => {})).toThrow();
  });
});

describe("fetchJson", () => {
  test("should fetch and parse json response", async () => {
    fetch.mockReturnValue(mockedResponse({ a: 1 }));

    const response = await fetchJson("/url");

    expect(response).toEqual({ a: 1 });
    expect(fetch).toHaveBeenCalledWith("/url", {
      method: "GET",
      params: {},
      mode: "cors",
    });
  });

  test("should format params given to as option", async () => {
    fetch.mockReturnValue(mockedResponse({ a: 1 }));

    const response = await fetchJson("/url", { params: { b: 1 } });

    expect(response).toEqual({ a: 1 });
    expect(fetch).toHaveBeenCalledWith("/url?b=1", {
      method: "GET",
      params: { b: 1 },
      mode: "cors",
    });
  });

  test("should fail on rejection", async () => {
    fetch.mockReturnValue(Promise.reject());

    let thrown = false;

    try {
      await fetchJson("/url", { params: { b: 1 } });
    } catch (error) {
      thrown = true;
    }

    expect(thrown).toBeTruthy();
  });
});
