const { test, expect, describe } = require("@jest/globals");
const fetch = require("node-fetch");
const { getIssues, getIssuesCount } = require("./issues");
const { mockedResponse, expectRejectedPromise } = require("./test-utils");

describe("getIssuesCount", () => {
  test("should fetch issues count", async () => {
    fetch.mockReturnValue(
      mockedResponse({
        total: 20,
      })
    );

    const count = await getIssuesCount("IC", "1004");

    expect(count).toBe(20);
  });

  test("should check arguments", async () => {
    fetch.mockReturnValue(
      mockedResponse({
        total: 20,
      })
    );

    expectRejectedPromise(getIssuesCount("IC", 1004));
    expectRejectedPromise(getIssuesCount("IC", undefined));
    expectRejectedPromise(getIssuesCount(null, "1004"));
  });

  test("should check response", async () => {
    fetch.mockReturnValueOnce(mockedResponse({}));

    expectRejectedPromise(getIssuesCount("IC", "1004"));

    fetch.mockReturnValueOnce(
      mockedResponse({
        total: "223",
      })
    );

    expectRejectedPromise(getIssuesCount("IC", "1004"));
  });
});

describe("getIssues", () => {
  test("should fetch issues", async () => {
    fetch.mockReturnValue(
      mockedResponse({
        total: 1,
        issues: [
          {
            fields: {
              components: [
                {
                  id: "some id",
                },
              ],
            },
          },
        ],
      })
    );

    const issues = await getIssues("IC");

    expect(issues).toEqual([
      {
        components: [
          {
            id: "some id",
          },
        ],
      },
    ]);
  });

  test("should fetch all issues", async () => {
    fetch.mockReturnValueOnce(
      mockedResponse({
        total: 2,
        issues: [
          {
            fields: {
              components: [
                {
                  id: "some id",
                },
              ],
            },
          },
        ],
      })
    );
    fetch.mockReturnValueOnce(
      mockedResponse({
        total: 2,
        issues: [
          {
            fields: {
              components: [
                {
                  id: "s2",
                },
              ],
            },
          },
        ],
      })
    );

    const issues = await getIssues("IC");

    expect(issues).toEqual([
      {
        components: [
          {
            id: "some id",
          },
        ],
      },
      {
        components: [
          {
            id: "s2",
          },
        ],
      },
    ]);
  });

  test("should fail on incorrect response", async () => {
    fetch.mockReturnValueOnce(
      mockedResponse({
        total: 1,
        issues: [
          {
            fie2lds: {
              components: [
                {
                  id: "some id",
                },
              ],
            },
          },
        ],
      })
    );

    await expectRejectedPromise(getIssues("IC"), "Value needs to be an object");

    fetch.mockReturnValueOnce(
      mockedResponse([
        {
          total: 1,
          issues: [
            {
              fields: {
                components: [
                  {
                    id: "some id",
                  },
                ],
              },
            },
          ],
        },
      ])
    );

    await expectRejectedPromise(getIssues("IC"), "Value needs to be an object");

    fetch.mockReturnValueOnce(
      mockedResponse({
        total: "fffd",
        issues: [
          {
            fields: {
              components: [
                {
                  id: "some id",
                },
              ],
            },
          },
        ],
      })
    );

    await expectRejectedPromise(
      getIssues("IC"),
      "Incorrect type of value number required"
    );
  });
});
