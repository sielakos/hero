jest.mock("./components");

const {
  test,
  expect,
  describe,
  beforeEach,
  afterEach,
} = require("@jest/globals");
const { printLeadlessSummary } = require("./summary");

describe("printLeadlessSummary", () => {
  let consoleLog;

  beforeEach(() => {
    consoleLog = console.log;

    console.log = jest.fn();
  });

  afterEach(() => {
    console.log = consoleLog;
  });

  test("should print summary", async () => {
    await printLeadlessSummary("IC");

    expect(console.log).toHaveBeenCalledWith(
      "id: 1, name: first, issues count: 3"
    );
  });
});
