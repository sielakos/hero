const { expect } = require("@jest/globals");
const { assertInstance, assertType } = require("./asserts");

/**
 * Just a helper function creating fake fetch response from given value
 * @param {*} value
 */
function mockedResponse(value) {
  return Promise.resolve({
    json: () => Promise.resolve(value),
  });
}

exports.mockedResponse = mockedResponse;

/**
 * Check if promise fails with expected error message
 *
 * @param {Promise} promise
 * @param {string} msg
 */
async function expectRejectedPromise(promise, msg) {
  assertInstance(promise, Promise);
  if (msg !== undefined) {
    assertType(msg, "string");
  }

  let error;

  try {
    await promise;
  } catch (err) {
    error = err;
  }

  assertInstance(error, Error);
  if (msg) {
    expect(error.message).toEqual(msg);
  } else {
    expect(error).toBeDefined();
  }
}
exports.expectRejectedPromise = expectRejectedPromise;
