const { test, expect, describe } = require("@jest/globals");
const {
  assertType,
  assertFunction,
  assertInstance,
  assertObject,
  assert,
  AssertionError,
} = require("./asserts");

describe("AssertionError", () => {
  test("should create new instance with proper message property", () => {
    const msg = "error msg 1";
    const instance = new AssertionError(msg);

    expect(instance.message).toBe(msg);
  });
});

describe("assert", () => {
  test("should throw if condition is falsy", () => {
    expect(assert(true, "err")).toBeUndefined();
    expect(assert("true", "err")).toBeUndefined();
    expect(() => assert("", "err")).toThrow("err");
    expect(() => assert(false, "err")).toThrow();
    expect(() => assert(undefined, "err")).toThrow();
    expect(() => assert(null, "err")).toThrow();
    expect(() => assert(0, "err")).toThrow();
  });

  test("should accept only string message", () => {
    expect(() => assert(0, 123)).toThrow(
      "Incorrect type of value string required"
    );
  });
});

describe("assertObject", () => {
  test("should assert that value is an object", () => {
    expect(assertObject({})).toBeUndefined();
    expect(() => assertObject(null)).toThrow("Value needs to be an object");
    expect(() => assertObject(undefined)).toThrow(
      "Value needs to be an object"
    );
    expect(() => assertObject(123)).toThrow("Value needs to be an object");
    expect(() => assertObject(() => {})).toThrow("Value needs to be an object");
    expect(() => assertObject([1, 2, 3])).toThrow(
      "Value needs to be an object"
    );
  });
});

describe("assertInstance", () => {
  test("should asset that value is instance of given class", () => {
    class SomeClass {}

    expect(assertInstance(new SomeClass(), SomeClass)).toBeUndefined();
    expect(() => assertInstance({}, SomeClass)).toThrow(
      "Value needs to be instance of BaseClass"
    );
  });

  test("should accept string message as third argument", () => {
    class SomeClass {}

    expect(() => assertInstance({}, SomeClass, "error-1")).toThrow("error-1");
    expect(() => assertInstance({}, SomeClass, 122)).toThrow(
      "Incorrect type of value string required"
    );
    expect(() => assertInstance({}, SomeClass, null)).toThrow(
      "Incorrect type of value string required"
    );
  });
});

describe("assertType", () => {
  test("should assert that value has given type", () => {
    expect(assertType("ala", "string")).toBeUndefined();

    expect(() => assertType({}, "string")).toThrow(
      "Incorrect type of value string required"
    );
  });

  test("should be resilient to wrong input", () => {
    expect(() => assertType("ala", null)).toThrow("Type needs to be string");
    expect(() => assertType("ala", undefined)).toThrow(
      "Type needs to be string"
    );
    expect(() => assertType("ala", 122)).toThrow("Type needs to be string");
    expect(() => assertType("ala", {})).toThrow("Type needs to be string");
    expect(() => assertType("ala", () => {})).toThrow(
      "Type needs to be string"
    );
  });
});

describe("assertFunction", () => {
  test("should assert that value is function", () => {
    expect(assertFunction(() => {})).toBeUndefined();
    expect(assertFunction(function () {})).toBeUndefined();
    expect(() => assertFunction(123)).toThrow("Value needs to be a function");
    expect(() => assertFunction([1, 2, 3])).toThrow(
      "Value needs to be a function"
    );
    expect(() => assertFunction({})).toThrow("Value needs to be a function");
    expect(() => assertFunction(null)).toThrow("Value needs to be a function");
    expect(() => assertFunction(undefined)).toThrow(
      "Value needs to be a function"
    );
  });
});
